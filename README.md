## CHALLENGE BACKEND

El programa tiene 3 archivos de Python para su funcionamiento. 

El archivo *funciones.py* contiene las funciones que son utilizadas a lo largo del programa. El *main.py* que esta diseñado para inicializar la base de datos con algunos inputs en las collections para su funcionamiento. Por último, el *user.py* está diseñado para ser utilizado por el usuario desde la consola. 