##################################################
## CHALLENGE BACKEND
##################################################
## Author: Agustin Mariano Rombola
## Email: rombola.agustin@gmail.com
## GitLab : gitlab.com/rombola.agustin
##################################################

# Se importan las librerias a utilizar
import funciones as ff
import datetime as dt
from pymongo import MongoClient
import numpy as np
#import unittest

# Conexion con el servidor de mongo
mongoServer = "mongodb://localhost"
client = MongoClient(mongoServer)

# Elimina la database por las dudas... (solo para testear)
try:
	client.drop_database('Gimnasio')
except:
	print('La base de datos no pudo ser eliminada porque no existe')

############################################################################
# CREACION DE LA BASE DE DATOS
############################################################################
db = client['Gimnasio']
socios = db['socios'] 
planes = db['planes'] 
descuentos = db['descuentos'] 
pagos = db['pagos']

############################################################################
# INGRESO DE DATOS (PARA PODER REALIZAR PRUEBAS)
############################################################################

# Para que los id sean con incremento automatico
id_socio = socios.count_documents({}) + 1
socios.insert_one({'_id':id_socio,'name':'juan','id_plan':1,'discounts_abs':[-100],'discounts_porcentaje':[0.2,0.2],'state':True,'fecha_vigencia':dt.datetime.now()})
id_socio = socios.count_documents({}) + 1
socios.insert_one({'_id':id_socio,'name':'agustina','id_plan':1,'discounts_abs':[-150],'discounts_porcentaje':[0.1,0.1],'state':True,'fecha_vigencia':dt.datetime.now()})

id_plan = planes.count_documents({}) + 1
planes.insert_one({'_id':id_plan,'name':'Plan Beginner','precio':3000})
id_plan = planes.count_documents({}) + 1
planes.insert_one({'_id':id_plan,'name':'Plan Ultimate','precio':5000})

id_descuento =  descuentos.count_documents({}) + 1
descuentos.insert_one({'_id':id_descuento,'porcentaje':0,'monto_absoluto':-100,'cant_aplicaciones':3}) #Es un descuento de 100 pesos por 3 meses
id_descuento =  descuentos.count_documents({}) + 1
descuentos.insert_one({'_id':id_descuento,'porcentaje':0.1,'monto_absoluto':0,'cant_aplicaciones':3}) #Descuento del 10% por 3 meses


############################################################################
# MAIN
############################################################################

ff.AplicarDescuento(1,1,socios,descuentos,'agregar')
ff.AplicarDescuento(1,2,socios,descuentos,'sumar')

