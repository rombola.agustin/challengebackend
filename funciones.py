##################################################
## CHALLENGE BACKEND
##################################################
## Author: Agustin Mariano Rombola
## Email: rombola.agustin@gmail.com
## GitLab : gitlab.com/rombola.agustin
##################################################

import datetime as dt
import numpy as np

############################################################################
# FUNCIONES
############################################################################

# Sumar de distintas longitud
def unequal_add(a,b):
	a = np.array(a,dtype='float')
	b = np.array(b,dtype='float')
	#print(a,b)
	if len(a) < len(b):
		c = b.copy()
		c[:len(a)] += a
	else:
		c = a.copy()
		c[:len(b)] += b
	return c.tolist()

# Aplica descuento por id_descuento y al socio con id_socio
def AplicarDescuento(id_socio,id_descuento,socios,descuentos,tipo): 
	print('APLICANDO DESCUENTOS...')

	# Obtiene los descuentos que ya tiene el socio	
	discount_socio_abs = socios.find_one({'_id':id_socio})['discounts_abs']
	discount_socio_porcentaje = socios.find_one({'_id':id_socio})['discounts_porcentaje']
	print('Descuentos disponibles del socio')
	print(f'Descuentos absolutos: {discount_socio_abs}')
	print(f'Descuentos porcentuales: {discount_socio_porcentaje}')

	# Toma la serie de descuentos a aplicar
	cant_aplicaciones = descuentos.find_one({'_id':id_descuento})['cant_aplicaciones']

	discount_abs = descuentos.find_one({'_id':id_descuento})['monto_absoluto']
	if discount_abs != 0:
		discount_abs = [discount_abs]*cant_aplicaciones
	else:
		discount_abs = []

	discount_porcentaje = descuentos.find_one({'_id':id_descuento})['porcentaje']
	if discount_porcentaje != 0:
		discount_porcentaje = [discount_porcentaje]*cant_aplicaciones
	else:
		discount_porcentaje = []
	# Aplica los descuentos en funcion de si se agregan o se suman
	if tipo == 'agregar':
		print('Agrega descuentos al final')
		new_abs = discount_socio_abs + discount_abs
		new_porcentaje = discount_socio_porcentaje + discount_porcentaje
	if tipo == 'sumar':
		print('Suma descuentos a los actuales')
		new_abs = unequal_add(discount_socio_abs,discount_abs)
		new_porcentaje = unequal_add(discount_socio_porcentaje,discount_porcentaje)
	# Actualiza la info
	socios.update_one({'_id':id_socio},{'$set':{'discounts_abs':new_abs}})
	socios.update_one({'_id':id_socio},{'$set':{'discounts_porcentaje':new_porcentaje}})

	print('DESCUENTOS APLICADOS CON EXITO')

# devuelve el Id del cliente y el monto a cobrar
def calcularMonto(socios,planes):
	allSocios = socios.find() 
	listado = {}
	for s in allSocios:
		id_socio = s['_id']
		id_plan = s['id_plan']
		precio = planes.find_one({'_id':id_plan})['precio']
		discount_abs = s['discounts_abs']
		discount_porcentaje = s['discounts_porcentaje']
		#print(precio)
		if len(discount_porcentaje) > 0: #Chequea si hay descuentos porcentuales
			precio = precio*(1-discount_porcentaje[0])
			#print(precio)
		if len(discount_abs) > 0: #Chequea si hay descuentos absolutos
			precio = precio+discount_abs[0]
			#print(precio)
		if precio < 0: # El precio no puede ser menor a cero
			precio = 0
		listado[id_socio]=precio
	return listado

# Realiza el pago consiste en utilizar la info de calcular monto y ejecutar el pago actualizando la fecha de vigencia y restando los desc a aplicar
def aplicarPago(listado,socios,pagos):
	for id_socio in listado:
		# Obtiene los descuentos que ya tiene el socio
		discount_socio_abs = socios.find_one({'_id':id_socio})['discounts_abs']
		discount_socio_porcentaje = socios.find_one({'_id':id_socio})['discounts_porcentaje']
		# Resta los descuentos que acaba de aplicar al array
		socios.update_one({'_id':id_socio},{'$set':{'discounts_abs':discount_socio_abs[1:]}})
		socios.update_one({'_id':id_socio},{'$set':{'discounts_porcentaje':discount_socio_porcentaje[1:]}})
		# Actualiza la fecha de vigencia
		delta = dt.timedelta(days=30)
		fecha_actual = socios.find_one({'_id':id_socio})['fecha_vigencia']
		socios.update_one({'_id':id_socio},{'$set':{'fecha_vigencia':fecha_actual+delta}})

		# Inserta el listado de pagos en la collections pagos 
		id_pago = pagos.count_documents({}) + 1
		add_pago = {'_id':id_pago,'id_socio':id_socio,'monto_pago':listado[id_socio],'fecha_de_pago':dt.datetime.now()}
		pagos.insert_one(add_pago)
	print('PAGOS APLICADOS CON EXITO')
# Muestra la información de las colections ingresadas	
def MostrarTodasCollections(array_collections):
	for c in array_collections:
		results = c.find() 
		for r in results:
			print(r)

# Muestra el listado de pagos con el nombre del socio
def verListadoPagos(socios,listado):
	for i in listado:
		name = socios.find_one({'_id':i})['name']
		print(f'Id: {i}, nombre: {name} debe pagar {listado[i]}')

# Agrega un socio nuevo
def addSocio(socios,planes):
	id_socio = socios.count_documents({}) + 1
	name = input('Nombre del socio: ')
	idCorrect = False
	while(idCorrect==False):
		id_plan = int(input('Id del plan: '))
		try:
			aux = planes.find_one({'_id':id_plan})
			if aux is not None:
				idCorrect = True
			else:
				print('El plan no existe')
				idCorrect = False
		except:
			idCorrect = False
			print('El plan no existe')
	socios.insert_one({'_id':id_socio,'name':name,'id_plan':id_plan,'discounts_abs':[],'discounts_porcentaje':[],'state':True,'fecha_vigencia':dt.datetime.now()})
