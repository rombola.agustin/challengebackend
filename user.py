##################################################
## CHALLENGE BACKEND
##################################################
## Author: Agustin Mariano Rombola
## Email: rombola.agustin@gmail.com
## GitLab : gitlab.com/rombola.agustin
##################################################

import datetime as dt
import numpy as np
from pymongo import MongoClient
import funciones as ff

# Conexion con el servidor de mongo
mongoServer = "mongodb://localhost"
client = MongoClient(mongoServer)

############################################################################
# CONEXION CON LA BASE DE DATOS Y SUS COLLECTIONS
############################################################################
db = client['Gimnasio']
socios = db['socios'] 
planes = db['planes'] 
descuentos = db['descuentos'] 
pagos = db['pagos']

############################################################################
# FUNCIONES
############################################################################

def printMenu():
	print('User program - Gym')
	print('-----------------------------------------')
	print('Para usar algunas de funcionalidades simplemente digite el numero indicado o escriba exit para salir')
	print('1 _ Mostrar todas las colecctions')
	print('2 _ Calcular el listado de pagos')
	print('3 _ Aplicar descuento')
	print('4 _ Agregar socio')
	print('5 _ Aplicar pagos (previamente se debe haber calculado el listado)')

############################################################################
# MAIN
############################################################################
print('¡ BIENVENIDO !')
userInput = 'enter'
listado_pagos = {}

while(userInput!='exit'):
	printMenu()
	userInput = input('Input: ')
	if userInput == '1':
		ff.MostrarTodasCollections([socios,descuentos,planes,pagos])
	elif userInput == '2':
		listado_pagos = ff.calcularMonto(socios,planes)
		ff.verListadoPagos(socios,listado_pagos)
	elif userInput == '3':	
		print('Queres que los descuentos se sumen (1) o se agreguen al final (2)?')
		opt = 3
		tipo = ''
		while(opt!='1' and opt!='2'):
			opt = input('_')
			if opt == '1':
				tipo = 'sumar'
			elif opt == '2':
				tipo = 'agregar'
		id_socio = int(input('Ingrese el id del socio: '))
		id_descuento = int(input('Ingrese el id del descuento: '))
		ff.AplicarDescuento(id_socio,id_descuento,socios,descuentos,tipo)
	elif userInput == '4':
		ff.addSocio(socios,planes)
	elif userInput == '5':
		if len(listado_pagos) == 0:
			print('No hay listado de pagos')
		else:
			ff.aplicarPago(listado_pagos,socios,pagos)
print('-----------------------------------------')
print('¡ HASTA LA PROXIMA !')