##################################################
## CHALLENGE BACKEND
##################################################
## Author: Agustin Mariano Rombola
## Email: rombola.agustin@gmail.com
## GitLab : gitlab.com/rombola.agustin
##################################################

# Se importan las librerias a utilizar
import datetime as dt
from pymongo import MongoClient
import numpy as np
import unittest

# Conexion con el servidor de mongo
mongoServer = "mongodb://localhost"
client = MongoClient(mongoServer)

# Elimina la database por las dudas... (solo para testear)
try:
	client.drop_database('Gimnasio')
except:
	print('La base de datos no pudo ser eliminada porque no existe')
############################################################################
# CREACION DE LA BASE DE DATOS
############################################################################
db = client['Gimnasio']
socios = db['socios'] 
planes = db['planes'] 
descuentos = db['descuentos'] 

############################################################################
# INGRESO DE DATOS (PARA PODER REALIZAR PRUEBAS)
############################################################################

# Para insertar informacion con nId auto increment
id_socio = socios.count_documents({}) + 1
id_plan = planes.count_documents({}) + 1
id_descuento =  descuentos.count_documents({}) + 1
socios.insert_one({'_id':id_socio,'name':'juan','id_plan':1,'discounts_abs':[-100],'discounts_porcentaje':[0.2,0.2],'state':True,'fecha_vigencia':dt.datetime.now()})
id_socio = socios.count_documents({}) + 1
socios.insert_one({'_id':id_socio,'name':'juan','id_plan':1,'discounts_abs':[-150],'discounts_porcentaje':[0.1,0.1],'state':True,'fecha_vigencia':dt.datetime.now()})
planes.insert_one({'_id':id_plan,'name':'Plan Beginner','precio':3000})
descuentos.insert_one({'_id':id_descuento,'porcentaje':0,'monto_absoluto':-100,'cant_aplicaciones':3}) #Es un descuento de 100 pesos por 3 meses
id_descuento =  descuentos.count_documents({}) + 1
descuentos.insert_one({'_id':id_descuento,'porcentaje':0.1,'monto_absoluto':0,'cant_aplicaciones':3}) #Descuento del 10% por 3 meses
#collection.insert.many([p1,p2,p3])

############################################################################
# FUNCIONES PARA APLICAR PLANES Y DESCUENTOS
############################################################################

# Sumar de distintas longitud
def unequal_add(a,b):
	a = np.array(a,dtype='float')
	b = np.array(b,dtype='float')
	print(a,b)
	if len(a) < len(b):
		c = b.copy()
		c[:len(a)] += a
	else:
		c = a.copy()
		c[:len(b)] += b
	return c.tolist()

# Aplica descuento por id_descuento y al socio con id_socio
def AplicarDescuento(id_socio,id_descuento,tipo): 
	print('APLICANDO DESCUENTOS...')

	# Obtiene los descuentos que ya tiene el socio	
	discount_socio_abs = socios.find_one({'_id':id_socio})['discounts_abs']
	discount_socio_porcentaje = socios.find_one({'_id':id_socio})['discounts_porcentaje']
	print('Descuentos disponibles del socio')
	print(f'Descuentos absolutos: {discount_socio_abs}')
	print(f'Descuentos porcentuales: {discount_socio_porcentaje}')

	# Toma la serie de descuentos a aplicar
	cant_aplicaciones = descuentos.find_one({'_id':id_descuento})['cant_aplicaciones']

	discount_abs = descuentos.find_one({'_id':id_descuento})['monto_absoluto']
	if discount_abs != 0:
		discount_abs = [discount_abs]*cant_aplicaciones
	else:
		discount_abs = []

	discount_porcentaje = descuentos.find_one({'_id':id_descuento})['porcentaje']
	if discount_porcentaje != 0:
		discount_porcentaje = [discount_porcentaje]*cant_aplicaciones
	else:
		discount_porcentaje = []
	# Aplica los descuentos en funcion de si se agregan o se suman
	if tipo == 'agregar':
		print('Agrega descuentos al final')
		new_abs = discount_socio_abs + discount_abs
		new_porcentaje = discount_socio_porcentaje + discount_porcentaje
	if tipo == 'sumar':
		print('Suma descuentos a los actuales')
		new_abs = unequal_add(discount_socio_abs,discount_abs)
		new_porcentaje = unequal_add(discount_socio_porcentaje,discount_porcentaje)
	# Actualiza la info
	socios.update_one({'_id':id_socio},{'$set':{'discounts_abs':new_abs}})
	socios.update_one({'_id':id_socio},{'$set':{'discounts_porcentaje':new_porcentaje}})

	print('DESCUENTOS APLICADOS CON EXITO')

# devuelve el Id del cliente y el monto a cobrar
def calcularMonto(socios,planes):
	allSocios = socios.find() 
	listado = {}
	for s in allSocios:
		id_socio = s['_id']
		id_plan = s['id_plan']
		precio = planes.find_one({'_id':id_plan})['precio']
		discount_abs = s['discounts_abs']
		discount_porcentaje = s['discounts_porcentaje']
		#print(precio)
		if len(discount_porcentaje) > 0: #Chequea si hay descuentos porcentuales
			precio = precio*(1-discount_porcentaje[0])
			#print(precio)
		if len(discount_abs) > 0: #Chequea si hay descuentos absolutos
			precio = precio+discount_abs[0]
			#print(precio)
		if precio < 0: # El precio no puede ser menor a cero
			precio = 0
		listado[id_socio]=precio
	return listado
# realiza el pago consiste en utilizar la info de calcular monto y ejecutar el pago actualizando la fecha de vigencia y restando los desc a aplicar
def aplicarPago(listado):
	for id_socio in listado:
		# Obtiene los descuentos que ya tiene el socio
		discount_socio_abs = socios.find_one({'_id':id_socio})['discounts_abs']
		discount_socio_porcentaje = socios.find_one({'_id':id_socio})['discounts_porcentaje']
		# Resta los descuentos que acaba de aplicar al array
		socios.update_one({'_id':id_socio},{'$set':{'discounts_abs':discount_socio_abs[1:]}})
		socios.update_one({'_id':id_socio},{'$set':{'discounts_porcentaje':discount_socio_porcentaje[1:]}})
		# Actualiza la fecha de vigencia
		socios.update_one({'_id':id_socio},{'$set':{'fecha_vigencia':dt.datetime.now()}})
def MostrarTodo(socios,descuentos,planes):
	# Devuelve una lista de resultados del find
	results_s = socios.find() #Dentro de find podria usar {} para buscar con alguna caracteristica especial...
	for r in results_s:
		print(r)

	results_p = planes.find() #Dentro de find podria usar {} para buscar con alguna caracteristica especial...
	for r in results_p:
		print(r)

	results_d = descuentos.find() #Dentro de find podria usar {} para buscar con alguna caracteristica especial...
	for r in results_d:
		print(r)
############################################################################
# PROGRAMA 
############################################################################

AplicarDescuento(1,1,'agregar')
AplicarDescuento(1,2,'sumar')

print('PAGOS:')
listado_pagos = calcularMonto(socios,planes)
for i in listado_pagos:
	print(listado_pagos[i])

print('ANTES DE APLICAR PAGO')

MostrarTodo(socios,descuentos,planes)

print('DESPUES DE APLICAR PAGO')
aplicarPago(listado_pagos)
MostrarTodo(socios,descuentos,planes)
#result = socios.delete_many({'name':'jorge'})

